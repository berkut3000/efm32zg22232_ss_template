/******************************************************************************
 * @ file efm32zg_reset_cause.c
 *
 *  Created on: 11/03/2019
 *      Author: AntoMota
 ******************************************************************************/

/* Headers */
#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_rmu.h"

/* HAL */
#include "usart_func.h"

/* Definitions */
#define SUCCESS 0

/* Functions */

/***************************************************************************//**
 * @brief
 *   Gets the Reset Cause Flags mask.
 *
 * @details
 *   Using EMlib fucntions, the Reset Cause Flags Mask is obtained, so its possi
 *      ble to determine the previous reset cause.
 *
 * @note
 *   None.
 *
 * @param[in] reset_flags
 *   A variable to store the current REset Flags Mask.
 *
 * @return
 *   0, if SUCCESS.
 ******************************************************************************/
uint32_t get_reset_cause()
{
    uint32_t reset_flags = RMU_ResetCauseGet();
    return reset_flags;
}


/***************************************************************************//**
 * @brief
 *   Prints the previously obtained reset mask through USART 0.
 *
 * @details
 *   After obtaining the Reset cause Flags Mask, you can print the results for
 *      for debugging purposes.
 *
 * @note
 *   Ensure to pass a valid value.
 *
 * @param[in] reset_flags
 *   The vairable containing the resulting flag mask
 *
 * @return
 *   0, if SUCCESS.
 *******    ***********************************************************************/
uint8_t print_reset_cause(uint32_t reset_flags)
{
    if (reset_flags & RMU_RSTCAUSE_PORST){
        uart_string_tx("Power On Rst\r\n");
    }
  //Brown Out Detector Unregulated Domain= BOD UD
    if (reset_flags & RMU_RSTCAUSE_BODUNREGRST){
        uart_string_tx("BOD UD Reset\r\n");
    }
    //Brown Out Detector Regulated Domain= BOD RD
    if (reset_flags & RMU_RSTCAUSE_BODREGRST){
        uart_string_tx("BOD RD Reset\r\n");
    }
    //External pin reset
    if (reset_flags & RMU_RSTCAUSE_EXTRST){
        uart_string_tx("Ext Pin Reset\r\n");
        //GPIO_PinModeSet(gpioPortC, 10, gpioModePushPull, 1);
    }
    if (reset_flags & RMU_RSTCAUSE_WDOGRST){
        uart_string_tx("Watchdog Reset\r\n");
    }
    if (reset_flags & RMU_RSTCAUSE_LOCKUPRST){
        uart_string_tx("LOCKUP Reset\r\n");
    }
    //System Request Reset
    if (reset_flags & RMU_RSTCAUSE_SYSREQRST){
        uart_string_tx("Syst Req Reset\r\n");
    }
    if (reset_flags & RMU_RSTCAUSE_EM4RST){
        uart_string_tx("EM4 Reset\r\n");
    }
    if (reset_flags & RMU_RSTCAUSE_EM4WURST){
        uart_string_tx("EM4 Wake-up Rst\r\n");
    }
    if (reset_flags & RMU_RSTCAUSE_BODAVDD0){
        uart_string_tx("AVDD0 Bod Rst\r\n");
    }
    if (reset_flags & RMU_RSTCAUSE_BODAVDD1){
        uart_string_tx("AVDD1 Bod Rst\r\n");
    }

    // Clear reset causes so we know which reset matters next time around
    RMU_ResetCauseClear();

    return SUCCESS;
}
