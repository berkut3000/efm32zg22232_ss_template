/******************************************************************************
 * @ file  leuart_func.c
 *
 *  Created on: 25/10/2018
 *  Modified on: 26/09/2019
 *      Author: AntoMota
 ******************************************************************************/
#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_dma.h"
#include "em_leuart.h"
#include "leuart_func.h"
#include "dmactrl.h"


#include "delay_func.h"

/* New data types */

/* Constant defintions */
#define LEUART_DMA_CHANNEL      0
#define BUFFERSIZE              100
#define LEUART_DMA_RX_TIMEOUT   10000 /* miliseconds */

/* Macro definitions */
#define SUCCESS  0  /* Exito */

/* Static Data */

/* Private function prototypes */

/* Public Functions */

/***************************************************************************//**
 * @brief  Setting up LEUART
 ******************************************************************************/
void leuart_setup(void)
{
    /* Enable peripheral clocks */
    CMU_ClockEnable(cmuClock_HFPER, true);
    
    /* Configure GPIO pins */
    CMU_ClockEnable(cmuClock_GPIO, true);
  
    LEUART_Init_TypeDef init = LEUART_INIT_DEFAULT;

    /* Enable CORE LE clock in order to access LE modules */
    CMU_ClockEnable(cmuClock_CORELE, true);

    /* Select LFXO for LEUARTs (and wait for it to stabilize) */
    CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_LFXO);
    CMU_ClockEnable(cmuClock_LEUART0, true);

    /* Do not prescale clock */
    CMU_ClockDivSet(cmuClock_LEUART0, cmuClkDiv_1);

    /* Configure LEUART */
    init.enable = leuartDisable;

    LEUART_Init(LEUART0, &init);

    /* Enable pins at default location */
    LEUART0->ROUTE = LEUART_ROUTE_RXPEN | LEUART_ROUTE_TXPEN | LEUART_LOCATION;

    /* Set RXDMAWU to wake up the DMA controller in EM2 */
    //LEUART_RxDmaInEM2Enable(LEUART0, true);

    /* Clear previous RX interrupts */
    LEUART_IntClear(LEUART0, LEUART_IF_RXDATAV);
    NVIC_ClearPendingIRQ(LEUART0_IRQn);

    /* Finally enable it */
    LEUART_Enable(LEUART0, leuartEnable);
  
    /* To avoid false start, configure output as high */
    GPIO_PinModeSet(LEUART_TXPORT, LEUART_TXPIN, gpioModePushPull, 1);
#if 0  
    GPIO_PinModeSet(LEUART_RXPORT, LEUART_RXPIN, gpioModeInput, 0);
#endif
#if 1
    GPIO_PinModeSet(LEUART_RXPORT, LEUART_RXPIN, gpioModeInputPull, 0);
#endif

    /* return SUCCESS; */
}

/***************************************************************************//**
 * @brief
 *   Configure DMA.
 *
 * @details
 *   This function initializes DMA controller.
 *   It configures the DMA channel to be used for LEUART0 transmit and receive.
 *   The primary descriptor for channel0 is configured for N_MINUS_1 byte 
 *   transfer. For continous data reception and transmission using LEUART in
 *   basic mode DMA Callback function is configured to reactivate basic transfer
 *   cycle once it is completed. 
 *
 * @note
 *   The function strlen from "string.h" is used for string lenght measurement.
 *
 * @param[in] data_string
 *   A pointer to the data.
 *
 * @return
 *   If succesful, a SUCCESS(0), is returned
 ******************************************************************************/
uint8_t leuart_dma_setup()
{
    /* DMA configuration structs */
    DMA_Init_TypeDef       dmaInit;
    DMA_CfgChannel_TypeDef channelCfg;
    DMA_CfgDescr_TypeDef   descrCfg;

    /* Initializing the DMA */
    dmaInit.hprot        = 0;
    dmaInit.controlBlock = dmaControlBlock;
    DMA_Init(&dmaInit);

    /* Set the interrupt callback routine */
    //dmaCallBack.cbFunc = basicTransferComplete;
    /* Callback doesn't need userpointer */
    //dmaCallBack.userPtr = NULL;

    /* Setting up channel */
    channelCfg.highPri   = false; /* Can't use with peripherals */
    channelCfg.enableInt = true;  /* Enabling interrupt to refresh DMA cycle*/

    /* Configure channel 0 */
    /*Setting up DMA transfer trigger request*/
    channelCfg.select = DMAREQ_LEUART0_RXDATAV;
    /* Setting up callback function to refresh descriptors*/
    channelCfg.cb     = NULL;
    DMA_CfgChannel(0, &channelCfg);

    /* Setting up channel descriptor */
    /* Destination is LEUART_Tx register and doesn't move */
    descrCfg.dstInc = dmaDataInc1 ;

    /* Source is LEUART_RX register and transfers 8 bits each time */
    descrCfg.srcInc = dmaDataIncNone;
    descrCfg.size   = dmaDataSize1;

    /* Default setting of DMA arbitration*/
    descrCfg.arbRate = dmaArbitrate1;
    descrCfg.hprot   = 0;

    /* Configure primary descriptor */
    DMA_CfgDescr(LEUART_DMA_CHANNEL, true, &descrCfg);
    return SUCCESS;
}

/***************************************************************************//**
 * @brief
 *   Transmit one String.
 *
 * @details
 *   Depending on the String length configuration, it is send through LEUART
 *      peripheral.
 *
 * @note
 *   The function strlen from "string.h" is used for string lenght measurement.
 *
 * @param[in] data_string
 *   A pointer to the data.
 *
 * @return
 *   If succesful, a SUCCESS(0), is returned
 ******************************************************************************/
uint8_t leuart_string_tx(uint8_t * data_string)
{   
    uint8_t tama = 0;
    tama = strlen((char *)data_string);
  
    for(int i = 0; i < tama; i++)
    {
        LEUART_Tx(LEUART0,(*(data_string+i)));
    }
    return SUCCESS;
}

/***************************************************************************//**
 * @brief
 *   Receive data.
 *
 * @details
 *   Receives through leuart suing DMA. The function times out after the
 *      specified number of miliseconds in delay_c.
 *
 * @note
 *  Ensure to setup DMA beforehand.
 *
 * @param[in] data_buff
 *   A pointer to the data buffer, where the data will be stored.
 *
 *
 * @return
 *   0, if SUCCESS
 ******************************************************************************/
uint8_t leuart_dma_rx(uint8_t * data_buff)
{

#if 1
    DMA_ActivateBasic(LEUART_DMA_CHANNEL,
    		                true,
    		                false,
    		                (void *)data_buff,
    		                (void *)&LEUART0->RXDATA,
    		                (BUFFERSIZE - 1));
#endif
    delay_c(LEUART_DMA_RX_TIMEOUT);
    DMA_ChannelEnable(LEUART_DMA_CHANNEL, false);
    return SUCCESS;
}

/***************************************************************************//**
 * @brief
 *   Receive data.
 *
 * @details
 *   Receives through leuart suing DMA. The function times out after the
 *      specified number of miliseconds in delay_c.
 *
 * @note
 *  Ensure to setup DMA beforehand.
 *
 * @param[in] data_buff
 *   A pointer to the data buffer, where the data will be stored.
 *
 * @param[in] time_out
 *   Miliseconds to wait before timeout.
 *
 * @return
 *   0, if SUCCESS
 ******************************************************************************/
uint8_t leuart_dma_rx_timeout(uint8_t * data_buff, uint16_t time_out)
{

#if 1
    DMA_ActivateBasic(LEUART_DMA_CHANNEL,
    		                true,
    		                false,
    		                (void *)data_buff,
    		                (void *)&LEUART0->RXDATA,
    		                (BUFFERSIZE - 1));
#endif
    delay_c(time_out);
    DMA_ChannelEnable(LEUART_DMA_CHANNEL, false);
    return SUCCESS;
}

/*************************** Deprecated ***************************************/

/***************************************************************************//**
 * @brief
 *   Receive one String.
 *
 * @details
 *   Processes the data received through LEUART, stores it in data_buff and is ..
 *   determined by data_buff_size.
 *
 * @note
 *   Buffer receive size is limited to data_buff_size bytes.
 *
 * @param[in] data_buff
 *   A pointer to the data Buffer.
 *
 * @param[in] data_buff_size
 *   The maximum size of the data to be received.
 *
 * @return
 *   If succesful, a SUCCESS(0), is returned
 ******************************************************************************/
uint8_t leuart_rx(uint8_t * data_buff, uint8_t data_buff_size)
{
    memset(data_buff, '\0', strlen((char *)data_buff));
    for(int i = 0; i < data_buff_size; i++)
    {
        *(data_buff+i) = LEUART_Rx(LEUART0);
//      if((*(data_buff+i)) == 0x0A)
//      {
//          return 0;
//      }
    }
  
    //artDelay(14);
  
    return SUCCESS;
}

/***************************************************************************//**
 * @brief
 *   Receive one String.
 *
 * @details
 *   Processes the data received through LEUART, stores it in data_buff and is ..
 *   determined by data_buff_size, but stops reception upon receiving a ..
 *   specified character.
 *
 * @note
 *   Buffer receive size is limited to 255 bytes.
 *
 * @param[in] data_buff
 *   A pointer to the data Buffer.
 *
 * @param[in] data_buff_size
 *   The maximum size of the data to be received.
 *
 * @param[in] dataStop
 *   Character which upon read, stops receiving and stores data.
 *
 * @return
 *   If succesful, a SUCCESS(0), is returned
 ******************************************************************************/
uint8_t leuart_rx_sc(uint8_t * data_buff, uint32_t data_buff_size,
        uint8_t data_stop, uint8_t data_stop_count)
{
    uint8_t data_stop_counter = 0;
    memset(data_buff, '\0', strlen((char *)data_buff));
    for(int i = 0; i < data_buff_size; i++)
    {
        *(data_buff+i) = LEUART_Rx(LEUART0);
        if((*(data_buff+i)) == data_stop)
        {
            data_stop_counter++;
            if(data_stop_count-1<data_stop_counter)
            {
                return SUCCESS;
            }
        }
   }
  
   return SUCCESS;
}
/* end of file */
