/******************************************************************************
 * @ file toascii.c
 *
 *  Created on: 13/06/2018
 *      Author: AntoMota
 ******************************************************************************/

/* Headers */
#include <toascii.h>
#include <string.h>

/* New data types */

/* Constant defintions */

/* Macro definitions */
#define SUCCESS         0

#define MS4B            0xF0
#define LS4B            0x0F

#define SIZE_DIGIT_HX   4
#define NINE_DIGIT      9
   
#define OFFSET_DIG      48
#define OFFSET_CAR      55

/* Static Data */

/* Private function prototypes */

/* Public Functions */

/***************************************************************************//**
 * @brief
 *  Converts Hex string to ascii.
 *
 * @details
 *   Converts Hex string to ascii string.
 *
 * @note
 *   For use for serial comm to RN2903, for example. All the data must be a
 *      printable ASCII hex string
 *
 * @param[in] data_string
 *   Pointer to string array containing the data to be converted..
 *
 * @return
 *   Zero if SUCCESS.
 ******************************************************************************/
uint8_t  hex_to_ascii(uint8_t * data_string)
{
    uint8_t aux_buff[20] = "";
    
    int8_t first_nibble = -1;
    int8_t last_nibble = -1;
    uint8_t package_size = 0;
  
    package_size = strlen((char*)data_string);
  
    for(int i = 0; i < package_size; i++)
    {
  
        /*Save the first 4 bits of the hexadecimal and the last 4 bits*/
        first_nibble = (*(data_string + i) & MS4B) >> SIZE_DIGIT_HX;
        last_nibble = *(data_string + i) & LS4B;
    
        /*Recover data and save in ascii format*/
    
        *(aux_buff + (2*i)) = first_nibble + (first_nibble > NINE_DIGIT ? OFFSET_CAR:OFFSET_DIG);
    
        *(aux_buff + 1 +(2*i)) = last_nibble + (last_nibble > NINE_DIGIT ? OFFSET_CAR:OFFSET_DIG);
    }
    memset(data_string, '\0', package_size);
    strcat((char *)&data_string[0], (const char *)(aux_buff));
        
    return SUCCESS;
}


/***************************************************************************//**
 * @brief
 *  Converts int to ascii.
 *
 * @details
 *   Converts int numbers to ascii string.
 *
 * @note
 *   For use for serial comm to RN2903, for example. All the array contents must
 *      valid number
 *
 * @param[in] number
 *   Pointer to number containing the data to be converted..
 *
 * @return
 *   Zero if SUCCESS.
 ******************************************************************************/
uint8_t int_to_ascii(uint8_t * number)
{
    return SUCCESS;
}

/* Private Functions */

/*** end of file ***/