/*
 * delay_func.c
 *
 *  Created on: 25/09/2018
 *      Author: AntoMota
 */

   /* Headers */
#include "delay_func.h"
#include "em_device.h"
//#include "intrinsics.h"

#define MS1000MHZ24    2650
#define MS1000MHZ14    1545
   
/***************************************************************************//**
 * @brief
 *   Generates a delay.
 *
 * @details
 *   Stops execution via assembly no operation instruction.
 *
 * @note
 *   Recommended to be replace with a Low-Power Mode timer.
 *
 * @param[in] nDelays
 *   Number of cycles the delay will ocurr.
 *
 * @return
 *   Zero if succes.
 ******************************************************************************/

uint8_t delay_c(uint32_t n_delays)
{
  for (uint32_t x = 0; x <= n_delays; x++)
  {
      for (uint32_t j = 0; j <= MS1000MHZ24; j++)
        {
            //__no_operation();
            __asm("nop");
        }
  }
  return 0;
}