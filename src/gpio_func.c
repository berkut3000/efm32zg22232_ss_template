/******************************************************************************
 * @ file  gpio_func.c
 *
 *  Created on: 25/10/2018
 *      Author: AntoMota
 ******************************************************************************/

/* Headers */
#include <stdbool.h>
#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_gpio.h"

#include "gpio_func.h"
#if 0
#include "delay_func.h"
#endif
/* New data types */



/* Definitions */
#define STK3200 1
#if STK3200
#define LED0_PORT       gpioPortC
#define LED0_PIN        10  /* PC8 in in Wristband, PC10 in STK_3200 */
#define LED1_PORT       gpioPortC
#define LED1_PIN        11  /* PC9 in in Wristband, PC11 in STK_3200 */
#define PBUTTON0_PORT   gpioPortC /* PortE in in Wristband, PortC in STK_3200 */
#define PBUTTON0_PIN    8   /* PE10 in in Wristband, PC8 in STK_3200 */
#elif
#define LED0_PORT       gpioPortC
#define LED0_PIN        8  /* PC8 in in Wristband, PC10 in STK_3200 */
#define LED1_PORT       gpioPortC
#define LED1_PIN        9  /* PC9 in in Wristband, PC11 in STK_3200 */
#define PBUTTON0_PORT   gpioPortE /* PortE in in Wristband, PortC in STK_3200 */
#define PBUTTON0_PIN    10   /* PE10 in in Wristband, PC8 in STK_3200 */
#endif

#define GPS_RESET_PORT  gpioPortA
#define GPS_RESET_PIN   10
#define GPS_WKU_PORT    gpioPortA
#define GPS_WKU_PIN     9
#define GPS_ONOFF_PORT  gpioPortC
#define GPS_ONOFF_PIN   2

#define RN2903_RESET_PORT gpioPortC
#define RN2903_RESET_PIN 3

/* Structs */
gpio_struct_t led0_struct = {LED0_PORT, LED0_PIN};
gpio_struct_t led1_struct = {LED1_PORT, LED1_PIN};

gpio_struct_t pbutton_struct = {PBUTTON0_PORT, PBUTTON0_PIN};

gpio_struct_t gps_reset_struct = {GPS_RESET_PORT,GPS_RESET_PIN};
gpio_struct_t gps_wku_struct = {GPS_WKU_PORT,GPS_WKU_PIN};
gpio_struct_t gps_onoff_struct = {GPS_ONOFF_PORT,GPS_ONOFF_PIN};
gpio_struct_t rn2903_reset_struct = {RN2903_RESET_PORT, RN2903_RESET_PIN};

/* Constant defintions */






/* Definitions */
#define SUCCESS 0
#define HIGH    1
#define LOW     0

/* Static Data */


/* Private function prototypes */

/* Public Functions */

/***************************************************************************//**
 * @brief
 *   Setup GPIO.
 *
 * @details
 *   Setup GPIO interrupt for pushbuttons and LED indicators.
 *
 * @note
 *   In the header files, reside the definitions for PORT and PINS.
 *
 * @param[in] none
 *
 * @return
 *   0, if SUCCESS
 ******************************************************************************/
uint8_t gpio_conf(void)
{
    /* Enable GPIO clock. */
    CMU_ClockEnable(cmuClock_GPIO, true);

    /* Configure PE10 as input and enable interrupt. */
    GPIO_PinModeSet(PBUTTON0_PORT, PBUTTON0_PIN, gpioModeInputPull, 1);
    GPIO_IntConfig(PBUTTON0_PORT, PBUTTON0_PIN, true, true, true);

    NVIC_ClearPendingIRQ(GPIO_EVEN_IRQn);
    NVIC_EnableIRQ(GPIO_EVEN_IRQn);

    NVIC_ClearPendingIRQ(GPIO_ODD_IRQn);
    NVIC_EnableIRQ(GPIO_ODD_IRQn);

    /* Configure LEDS */
    GPIO_PinModeSet(LED0_PORT,LED0_PIN,gpioModePushPull,1);
    GPIO_PinModeSet(LED1_PORT,LED1_PIN,gpioModePushPull,1);
    
    
    /* Configure GPS */
    GPIO_PinModeSet(GPS_ONOFF_PORT,GPS_ONOFF_PIN,gpioModePushPull,1);
    GPIO_PinModeSet(GPS_RESET_PORT,GPS_RESET_PIN,gpioModePushPull,1);
    GPIO_PinModeSet(GPS_WKU_PORT,GPS_WKU_PIN, gpioModeInputPull,1);
    
    GPIO_PinOutClear(GPS_RESET_PORT,GPS_RESET_PIN);
#if 0
    delay_c(3000);
#endif
    GPIO_PinOutSet(GPS_RESET_PORT,GPS_RESET_PIN);
    
#if 0
    while(SUCCESS == gpio_get(&gps_wku_struct))
    {
        GPIO_PinOutClear(GPS_ONOFF_PORT,GPS_ONOFF_PIN);
        delay_c(50);
        GPIO_PinOutSet(GPS_ONOFF_PORT,GPS_ONOFF_PIN);
        delay_c(50);
        GPIO_PinOutClear(GPS_ONOFF_PORT,GPS_ONOFF_PIN);
    }
#endif    
    /* Configure Lora Reset Pin */
#if 0
    GPIO_PinModeSet(RN2903_RESET_PORT, RN2903_RESET_PIN,gpioModePushPull,1);
    delay_c(255);
    delay_c(255);
    GPIO_PinOutClear(RN2903_RESET_PORT, RN2903_RESET_PIN);
    delay_c(255);
    delay_c(255);
    GPIO_PinOutSet(RN2903_RESET_PORT, RN2903_RESET_PIN);
    delay_c(255);
    delay_c(255);
#endif
    
    gpio_set(&led0_struct,HIGH);
    gpio_set(&led0_struct,LOW);
    gpio_set(&led1_struct,HIGH);
    gpio_set(&led1_struct,LOW);
    gpio_toggle(&led1_struct);
    gpio_toggle(&led0_struct);
    gpio_toggle(&led1_struct);
    gpio_toggle(&led0_struct);
    

    return SUCCESS;
}

/***************************************************************************//**
 * @brief
 *    Sets GPIO.
 *
 * @details
 *    Sets GPIO to the specified state.
 *
 * @note
 *    In the header files, reside the definitions for PORT and PINS.
 *
 * @param[in] gpio_pin
 *    The pin to be driven.
 *
 * @param[in] gpio_state
 *    The state that the pin will be driven to.
 *
 * @return
 *   0, if SUCCESS
 ******************************************************************************/
uint8_t gpio_set(gpio_struct_t * gpio_pin, bool gpio_state)
{
    switch(gpio_state)
    {
        case HIGH:
            GPIO_PinOutSet(gpio_pin->port, gpio_pin->pin);
            break;
        case LOW:
            /* Pin LOW */
            GPIO_PinOutClear(gpio_pin->port, gpio_pin->pin);
            break;
        default:
            /* Pin LOW */
            GPIO_PinOutClear(gpio_pin->port, gpio_pin->pin);
            break;
    }
    
    return SUCCESS;    
}

/***************************************************************************//**
 * @brief
 *    Gets GPIO.
 *
 * @details
 *    Gets state of the specified GPIO.
 *
 * @note
 *    In the header files, reside the definitions for PORT and PINS.
 *
 * @param[in] gpio_pin
 *    The pin to be read.
 *
 * @param[in] gpio_state
 *    The state that the pin will be driven to.
 *
 * @return
 *   0, if SUCCESS
 ******************************************************************************/
uint8_t gpio_get(gpio_struct_t * gpio_pin)
{    
    return GPIO_PinInGet(gpio_pin->port, gpio_pin->pin);    
}

/***************************************************************************//**
 * @brief
 *    Toggles GPIO.
 *
 * @details
 *    Changes the current GPIO state to the other value.
 *
 * @note
 *
 * @param[in] gpio_pin
 *    Struct to pin to be toggled.
 *
 * @return
 *   0 if SUCCESS.
 ******************************************************************************/
uint8_t gpio_toggle(gpio_struct_t * gpio_pin)
{
    GPIO_PinOutToggle(gpio_pin->port, gpio_pin->pin);
    return SUCCESS;
}

/***************************************************************************//**
 * @brief
 *   Unified GPIO Interrupt handler (pushbuttons)
 *      PB0 Starts selected test
 *      PB1 Cycles through the available tests
 * @details
 *   Whether the interrupt comes from an ODD or an EVEN pin, this function
        handles both.
 *
 * @note
 *   Remember to clear the interrupt mask.
 *
 * @param[in] dataString
 *   Values passed as parameters to the function.
 *
 * @return
 *   Nothing, it's an Interrupt function, for fuck's sake!
 ******************************************************************************/
void GPIO_Unified_IRQ(void)
{
  /* Get and clear all pending GPIO interrupts */
  uint32_t interruptMask = GPIO_IntGet();
  GPIO_IntClear(interruptMask);

  /* Act on interrupts */
  if (interruptMask & (1 << 8)) {
    /* ALT_PB */
    // enterEM4 = true;
  }
}

/***************************************************************************//**
 * @brief GPIO Interrupt handler for even pins
 *****************************************************************************/
void GPIO_EVEN_IRQHandler(void)
{
  GPIO_Unified_IRQ();
}

/***************************************************************************//**
 * @brief GPIO Interrupt handler for odd pins
 *****************************************************************************/
void GPIO_ODD_IRQHandler(void)
{
  GPIO_Unified_IRQ();
}