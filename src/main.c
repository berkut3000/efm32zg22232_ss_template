/******************************************************************************
 * @ file  main.c
 *  
 *
 *  Created on: 26/09/2019
 *  Modified on: 26/09/2019
 *      Author: AntoMota
 ******************************************************************************/
#include "em_device.h"
#include "em_chip.h"
#include "stdint.h"
#include "gpio_func.h"
#include "leuart_func.h"

int main(void)
{
    /* Chip errata */
    CHIP_Init();

    uint8_t hola_var = 0;
    gpio_conf();
    leuart_setup();
    uint8_t test_string[5] = "Hola";
    /* Infinite loop */
    while (1) {
        gpio_toggle(&led0_struct);
        leuart_string_tx(test_string);
    }
}
