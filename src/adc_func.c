/******************************************************************************
 * @ file adc_func.c
 *
 *  Created on: 11/10/2019
 *  Modified on: 11/10/2019
 *      Author: AntoMota
 ******************************************************************************/

/* Headers */
#include "adc_func.h"

#include "em_adc.h"
#include "em_cmu.h"


/* New data types */

/* Constant defintions */

/* Macro definitions */
#define SUCCESS		0

/* Static Data */

/* Private function prototypes */

/* Public Functions */

/***************************************************************************//**
 * @brief
 *   Brief description of the function.
 *
 * @details
 *   Detailed descritpion of the function.
 *
 * @note
 *   Comments for things that may not be intuitive.
 *
 * @param[in] dataString
 *   Values passed as parameters to the function.
 *
 * @return
 *   What the function yields.
 ******************************************************************************/
uint8_t adc_conf()
{
  /* Enable ADC clock */
  CMU_ClockEnable(cmuClock_ADC0, true);

  /* Base the ADC configuration on the default setup. */
  ADC_Init_TypeDef       init  = ADC_INIT_DEFAULT;
  ADC_InitSingle_TypeDef sInit = ADC_INITSINGLE_DEFAULT;

  /* Initialize timebases */
  init.timebase = ADC_TimebaseCalc(0);
  init.prescale = ADC_PrescaleCalc(400000, 0);
  ADC_Init(ADC0, &init);

  /* Set input to temperature sensor. Reference must be 1.25V */
  sInit.reference = adcRef1V25;
  sInit.input     = adcSingleInpTemp;
  ADC_InitSingle(ADC0, &sInit);

  return SUCCESS;
}


/***************************************************************************//**
 * @brief
 *   Brief description of the function.
 *
 * @details
 *   Detailed descritpion of the function.
 *
 * @note
 *   Comments for things that may not be intuitive.
 *
 * @param[in] dataString
 *   Values passed as parameters to the function.
 *
 * @return
 *   What the function yields
 ******************************************************************************/
uint32_t adc_read()
{
	{
		ADC_Start(ADC0, adcStartSingle);
		while ( (ADC0->STATUS & ADC_STATUS_SINGLEDV) == 0 ) {
		}
		return ADC_DataSingleGet(ADC0);
	}
}

/* Private Functions */

/*** end of file ***/
