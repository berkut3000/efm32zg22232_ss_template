/******************************************************************************
 * @ file systick_func.c
 *
 *  Created on: 20/09/2018
 *      Author: AntoMota
 ******************************************************************************/

/* Headers */
#include <systick_func.h>
#include "em_cmu.h"
#if 0
#include "gpio_func.h" /* For use of leds in SysTick interrupt handler */
#endif
/* New data types */

/* Constant defintions */
#define SUCCESS 0
#define HIGH    1
#define LOW     0
/* Macro definitions */

/* Static Data */
static volatile uint32_t msTicks; /* counts 1ms timeTicks */

/* Private function prototypes */

/* Public Functions */

/***************************************************************************//**
 * @brief
 *   Configures the systick.
 *
 * @details
 *   Configures and starts the SysTick Timer.
 *
 * @note
 *   The input parameter is the divider for the maximum available value
 *      depending on the used frequency. This value will determine the number of
 *      ticks between, a value which cannot surpass 0xFFFFFUL. The divider
 *      cannot be less than 0 nor bigger than the aforementioned hexadecimal
 *      value.     
 *
 * @param[in] divider
 *   Divider for the maximum value .
 *
 * @return
 *   0, if SUCCESS.
 ******************************************************************************/
uint8_t systick_conf(uint32_t divider)
{

  
    while(SUCCESS != SysTick_Config(SysTick_LOAD_RELOAD_Msk/divider))
    {
        asm("nop");
    }
    return SUCCESS;
}


/***************************************************************************//**
 * @brief
 *   Brief description of the function.
 *
 * @details
 *   Detailed descritpion of the function.
 *
 * @note
 *   Comments for things that may not be intuitive.
 *
 * @param[in] dataString
 *   Values passed as parameters to the function.
 *
 * @return
 *   What the function yields
 ******************************************************************************/
uint8_t systick_toggle(bool systick_status)
{
    if(HIGH == systick_status)
    {
        SysTick->CTRL  = 0x07;
    }else{
        SysTick->CTRL  &= 0x04;
    }
    return SysTick->CTRL;

}

/* Private Functions */
#if 0
/***************************************************************************//**
 * @brief SysTick_Handler
 *  Interrupt Service Routine for system tick counter
 *
 * @note
 *   THis is just left as a reference, paste this to where it would be nedded
 *      with custom code.
 *
 ******************************************************************************/
void SysTick_Handler(void)
{
    msTicks++;       /* increment counter necessary in Delay()*/
    gpio_toggle(&led1_struct);
}
#endif
/*** end of file ***/