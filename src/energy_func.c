/******************************************************************************
 * @ file energy_func.c
 *
 *  Created on: 20/09/2019
 *  Modified on: 20/09/2019
 *      Author: AntoMota
 ******************************************************************************/

/* Headers */
#include "energy_func.h"
#include "em_emu.h"
#include "stdbool.h"

/* New data types */

/* Constant defintions */

/* Macro definitions */
#define SUCCESS     0
/* Static Data */

/* Private function prototypes */

/* Public Functions */

/***************************************************************************//**
 * @brief
 *   Enter Energy mode 1 (Sleep).
 *
 * @details
 *   Enter Energy Mode 1.
 *
 * @note
 *   Remember to set a way to wake up.
 *
 * @param[in] none
 *
 * @return
 *   0, if SUCCESS.
 ******************************************************************************/
uint8_t energy_mode_1()
{
    EMU_EnterEM1();
    return SUCCESS;
}


/***************************************************************************//**
 * @brief
 *   Enter Energy mode 2 (DeepSleep).
 *
 * @details
 *   Enter Energy Mode 1.
 *
 * @note
 *   Remember to set a way to wake up.
 *
 * @param[in] none
 *
 * @return
 *   0, if SUCCESS.
 ******************************************************************************/
uint8_t energy_mode_2()
{
    EMU_EnterEM2(true);
    return SUCCESS;
}

/***************************************************************************//**
 * @brief
 *   Enter Energy mode 3 (Stop).
 *
 * @details
 *   Enter Energy Mode 3.
 *
 * @note
 *   Remember to set a way to wake up.
 *
 * @param[in] none
 *
 * @return
 *   0, if SUCCESS.
 ******************************************************************************/
uint8_t energy_mode_3()
{
    EMU_EnterEM3(true);
    return SUCCESS;
}

/***************************************************************************//**
 * @brief
 *   Enter Energy mode 4 (Shutoff).
 *
 * @details
 *   Enter Energy Mode 4.
 *
 * @note
 *   Remember to set a way to wake up.
 *
 * @param[in] none
 *
 * @return
 *   0, if SUCCESS.
 ******************************************************************************/
uint8_t energy_mode_4()
{
    EMU_EnterEM4();
    return SUCCESS;
}

/* Private Functions */

/*** end of file ***/