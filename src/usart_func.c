/******************************************************************************
 * @ file usart_func.c
 *
 *  Created on: 20/11/2018
 *      Author: Ernesto Moises
 ******************************************************************************/

/* Headers */
#include "em_device.h"
#include "em_chip.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_usart.h"

//#include "em_wdog.h"
#include "usart_func.h"

/* Definitions */
#define BUFFERSIZE      100
#define SUCCESS         0
#define TIME_OUT        1000000
#define NO_DATA         1

/* Global variables */
/* TX */
uint8_t buffer_uart_tx[BUFFERSIZE];
volatile bool usart_tx_flag = 0;
volatile static uint8_t buff_tx_ap = 0;
static uint8_t buff_tx_len = 0;
/* RX */
uint8_t buffer_uart_rx[BUFFERSIZE];
volatile bool usart_rx_flag = 0;
volatile static uint8_t buff_rx_ap = 0;
//static uint8_t buff_rx_len = 0;
volatile static uint8_t buff_rx_cnt = 0;

/* Setup UART2 in async mode for RS232*/
static USART_TypeDef           * Usart1_Port   = USART1;


/* Functions */
/***************************************************************************//**
 * @brief
 *      Setup Clocks.
 *
 * @details
 *      Calls functions from the EMLIB API to configure the Clock Managment Unit
 *          for USART module.
 *
 * @note
 *      Always call this fucntion before using the module. 
 *
 * @param[in] none
 *
 * @return
 *      Zero if SUCCESS.
 ******************************************************************************/
uint8_t cmu_setup()
{
    /* Select HFXO as clock source for HFCLK */
    CMU_ClockSelectSet(cmuClock_HF, cmuSelect_HFXO );
    CMU_ClockEnable( cmuClock_CORELE, true);

    /* Enable clock for HF peripherals */
    CMU_ClockEnable(cmuClock_HFPER, true);

    /* Enable clock for USART module */
    CMU_ClockEnable(cmuClock_USART1, true);

#if 0
    uint32_t clk_freq = CMU_ClockFreqGet(cmuClock_CORELE);
    clk_freq = CMU_ClockFreqGet(cmuClock_HFPER);
#endif
  
  return SUCCESS;
}

/***************************************************************************//**
 * @brief
 *      Sets USART module up.
 *
 * @details
 *      Sets USART module for UART up.
 *
 * @note
 *      Always call this fucntion before using the module. 
 *
 * @param[in] none
 *
 * @return
 *      Zero if SUCCESS.
 ******************************************************************************/
uint8_t uart_setup(uint32_t * bd, uint8_t * loc)
{
    /* Enable clock for GPIO module (required for pin configuration) */
    CMU_ClockEnable(cmuClock_GPIO, true);
  
    /* Configure GPIO pins Location #X*/
    switch(*loc)
    {
        case 1:
            GPIO_PinModeSet(gpioPortC, 0, gpioModePushPull, 1);   //TX
            GPIO_PinModeSet(gpioPortC, 1, gpioModeInput, 0);    //RX
            break;
        case 2:
            GPIO_PinModeSet(gpioPortC, 0, gpioModePushPull, 1);   //TX
            GPIO_PinModeSet(gpioPortC, 1, gpioModeInput, 0);    //RX
            break;
        case 3:
            GPIO_PinModeSet(gpioPortD, 7, gpioModePushPull, 1);   //TX
            GPIO_PinModeSet(gpioPortD, 6, gpioModeInput, 0);      //RX
            break;
    }
    
    /* Prepare struct for initializing UART in asynchronous mode*/
    static USART_InitAsync_TypeDef uart_init = USART_INITASYNC_DEFAULT;
    
    uart_init.enable       = usartDisable;   /* Don't enable UART upon intialization */
    uart_init.refFreq      = 0;              /* Provide information on reference frequency. When set to 0, the reference frequency is */
    uart_init.baudrate     = *bd;           /* Baud rate */
    uart_init.oversampling = usartOVS16;     /* Oversampling. Range is 4x, 6x, 8x or 16x */
    uart_init.databits     = usartDatabits8; /* Number of data bits. Range is 4 to 10 */
    uart_init.parity       = usartNoParity;  /* Parity mode */
    uart_init.stopbits     = usartStopbits1; /* Number of stop bits. Range is 0 to 2 */
    uart_init.mvdis        = false;          /* Disable majority voting */
    uart_init.prsRxEnable  = false;          /* Enable USART Rx via Peripheral Reflex System */
    uart_init.prsRxCh      = usartPrsRxCh0;  /* Select PRS channel if enabled */

    /* Initialize USART with uart_init struct */
    USART_InitAsync(Usart1_Port, &uart_init);

    /* Prepare UART Rx and Tx interrupts */
    USART_IntClear(Usart1_Port, _USART_IFC_MASK);
    //USART_IntEnable(Usart1_Port, USART_IEN_RXDATAV);
    NVIC_ClearPendingIRQ(USART1_RX_IRQn);
    NVIC_ClearPendingIRQ(USART1_TX_IRQn);
    NVIC_EnableIRQ(USART1_RX_IRQn);
    NVIC_EnableIRQ(USART1_TX_IRQn);

    /* Enable I/O pins at UART2 location #2 */
    
    switch(*loc)
    {
        case 1:
            Usart1_Port->ROUTE = USART_ROUTE_RXPEN |
                USART_ROUTE_TXPEN | USART_ROUTE_LOCATION_LOC0;
            break;
        case 2:
            Usart1_Port->ROUTE = USART_ROUTE_RXPEN |
                USART_ROUTE_TXPEN | USART_ROUTE_LOCATION_LOC0;
            break;
        case 3:
            Usart1_Port->ROUTE = USART_ROUTE_RXPEN |
                USART_ROUTE_TXPEN | USART_ROUTE_LOCATION_LOC3;
            break;
        
    }

    /* Enable UART */
    USART_Enable(Usart1_Port, usartEnable);

    return 0;
}

/***************************************************************************//**
 * @brief
 *      Sends Bytes.
 *
 * @details
 *      Sends Byte array through USART1.
 *
 * @note
 *      If you require to send NULLS, use this function. 
 *
 * @param[in] data_string
 *      Pointer to array to be sent.
 *
 * @param[in] data_len
 *      Pointer to variable representing the length of the String.
 *
 * @return
 *      Zero if SUCCESS.
 ******************************************************************************/
uint8_t uart_bytes_tx(uint8_t * data_string, uint8_t * data_len )
{
    while(false != usart_tx_flag);
    memset(buffer_uart_tx, '\0', BUFFERSIZE);
    strcat((char *)&buffer_uart_tx[0], (const char *)data_string);
    buff_tx_len = *data_len;
    usart_tx_flag = true;

    /* Enable interrupt on USART TX Buffer*/
    USART_IntEnable(Usart1_Port, USART_IEN_TXBL);
  
    return SUCCESS;
}

/***************************************************************************//**
 * @brief
 *      Sends String.
 *
 * @details
 *      Sends String array through USART1.
 *
 * @note
 *      None
 *
 * @param[in] data_string
 *      Pointer to array to be sent.
 *
 * @return
 *      Zero if SUCCESS.
 ******************************************************************************/
uint8_t uart_string_tx(uint8_t * data_string)
{
    
    while(false != usart_tx_flag);
    memset(buffer_uart_tx, '\0', BUFFERSIZE);
    strcat((char *)&buffer_uart_tx[0], (const char *)data_string);
    buff_tx_len = strlen((char *)data_string);
    usart_tx_flag = true;

    /* Enable interrupt on USART TX Buffer*/
    USART_IntEnable(Usart1_Port, USART_IEN_TXBL);
  
    return SUCCESS;
}

/***************************************************************************//**
 * @brief
 *      Receives data.
 *
 * @details
 *      Copies data from the RX buffer into an array specified by a pointer. If
 *          no data is received, the function will hang. While receiving, it
 *          it will expect a certian number of bytes defined by the BUFFERSIZE
 *          macro. If no data is received, the string will copy only the
 *          received bytes.
 *
 * @note
 *      A data must be received to exit from this function. Use this fucntion to
 *          ensure that data will be received as the MCU won't do anything else.
 *
 * @param[in] data_string
 *      Pointer to array to store the received data.
 *
 * @return
 *      Zero if SUCCESS.
 ******************************************************************************/
uint8_t uart_rx(uint8_t * data_string)
{
    uint8_t buff_rx_sp = buff_rx_ap;
    USART_IntEnable(Usart1_Port, USART_IEN_RXDATAV);
    while(true != usart_rx_flag);
    memset(data_string, '\0', BUFFERSIZE);
    
    uint32_t time_out = 0;
    while(BUFFERSIZE > buff_rx_cnt)
    {
        time_out++;
        if(TIME_OUT <= time_out)
        {
            buff_rx_cnt = BUFFERSIZE;
        }
        
    }
    
    uint8_t buff_rx_fp = buff_rx_ap;
    uint8_t aux_buff_0[BUFFERSIZE + 10] = "";
    uint8_t aux_buff_1[BUFFERSIZE + 10] = "";
    
    if(buff_rx_fp > buff_rx_sp)
    {
        strcat((char *)&aux_buff_0[0], (const char *)(buffer_uart_rx+buff_rx_sp));
        memset((aux_buff_0 + buff_rx_fp - buff_rx_sp) , '\0', (BUFFERSIZE - buff_rx_fp)  );
        
    }else{
        strcat((char *)&aux_buff_0[0], (const char *)(buffer_uart_rx + buff_rx_sp));
        memset((aux_buff_0 + strlen((char *)aux_buff_0) - 2),'\0', 2);
        strcat((char *)&aux_buff_1[0], (const char *)buffer_uart_rx);
        memset((aux_buff_1 + buff_rx_fp), '\0', (BUFFERSIZE - buff_rx_fp));
        strcat((char *)&aux_buff_0[0], (const char *)aux_buff_1);
        
    }
    
    
    strcat((char *)&data_string[0], (const char *)aux_buff_0);
    
    USART_IntDisable(Usart1_Port, USART_IEN_RXDATAV);
    buff_rx_cnt = 0;
    usart_rx_flag = false;
    
    return SUCCESS;
}

/***************************************************************************//**
 * @brief
 *      Receives data.
 *
 * @details
 *      As the previous function, the only difference is that the timeout setup
 *          to be freely chosen by the user. Copies data from the RX buffer into
 *          an array specified by a pointer. If no data is received, the
 *          function will hang. While receiving, it it will expect a certian
 *          number of bytes defined by the BUFFERSIZE macro. If no data is
 *          received, the string will copy only the received bytes.
 *
 * @note
 *      A data must be received to exit from this function. Use this fucntion to
 *          ensure that data will be received as the MCU won't do anything else.
 *
 * @param[in] data_string
 *      Pointer to array to store the received data.
 *
 * @param[in] t_out
 *      Pointer to timeout definition.
 *
 * @return
 *      Zero if SUCCESS.
 ******************************************************************************/
uint8_t uart_rx_timeout(uint8_t * data_string, uint32_t * t_out)
{
    USART_IntEnable(Usart1_Port, USART_IEN_RXDATAV);
    while(true != usart_rx_flag);
    memset(data_string, '\0', BUFFERSIZE);
    
    uint32_t time_out = 0;
    while(BUFFERSIZE > buff_rx_cnt)
    {
        time_out++;
        if(*t_out <= time_out)
        {
            buff_rx_cnt = BUFFERSIZE;
        }
        
    }
    
    strcat((char *)&data_string[0], (const char *)buffer_uart_rx);
    
    USART_IntDisable(Usart1_Port, USART_IEN_RXDATAV);
    buff_rx_cnt = 0;
    usart_rx_flag = false;
    
    return SUCCESS;
}

/***************************************************************************//**
 * @brief
 *      Receives data without hanging.
 *
 * @details
 *      Copies data from the RX buffer into an array specified by a pointer. If
 *          no data is received, the function will timeour. While receiving, it
 *          it will expect a certian number of bytes defined by the BUFFERSIZE
 *          macro. If no data is received, the string will copy only the
 *          received bytes.
 *
 * @note
 *      A data must be received to exit from this function. Use this fucntion to
 *          ensure that data will be received as the MCU won't do anything else.
 *
 * @param[in] data_string
 *      Pointer to array to store the received data.
 *
 * @return
 *      Zero if SUCCESS.
 ******************************************************************************/
uint8_t uart_rx_no_hang(uint8_t * data_string)
{
    USART_IntEnable(Usart1_Port, USART_IEN_RXDATAV);
    uint32_t time_out = 0;
    while(true != usart_rx_flag)
    {
        time_out++;
        if(TIME_OUT*2 <= time_out)
        {
            return NO_DATA;
        }
    }
    memset(data_string, '\0', BUFFERSIZE);
    
    time_out = 0;
    while(BUFFERSIZE > buff_rx_cnt)
    {
        time_out++;
        if(TIME_OUT <= time_out)
        {
            buff_rx_cnt = BUFFERSIZE;
        }
        
    }
    
    strcat((char *)&data_string[0], (const char *)buffer_uart_rx);
    
    USART_IntDisable(Usart1_Port, USART_IEN_RXDATAV);
    buff_rx_cnt = 0;
    usart_rx_flag = false;
    
    return SUCCESS;
}

/***************************************************************************//**
 * @brief
 *      Receives data without hanging.
 *
 * @details
 *      As the previous function, the only difference is that the timeout setup
 *          to be freely chosen by the user. Copies data from the RX buffer into
 *          an array specified by a pointer. If no data is received, the
 *          function will hang. While receiving, it it will expect a certain
 *          number of bytes defined by the BUFFERSIZE macro. If no data is
 *          received, the string will copy only the received bytes.
 *
 * @note
 *      A data must be received to exit from this function. Use this function to
 *          ensure that data will be received as the MCU won't do anything else.
 *
 * @param[in] data_string
 *      Pointer to array to store the received data.
 *
 * @param[in] t_out
 *      Pointer to timeout definition.
 *
 * @return
 *      Zero if SUCCESS.
 ******************************************************************************/
uint8_t uart_rx_timeout_no_hang(uint8_t * data_string, uint32_t * t_out)
{
    USART_IntEnable(Usart1_Port, USART_IEN_RXDATAV);
    uint32_t time_out = 0;
    while(true != usart_rx_flag)
    {
        time_out++;
        if(TIME_OUT*2 <= time_out)
        {
            return NO_DATA;
        }
    }
    memset(data_string, '\0', BUFFERSIZE);
    
    time_out = 0;
    while(BUFFERSIZE > buff_rx_cnt)
    {
        time_out++;
        if(*t_out <= time_out)
        {
            buff_rx_cnt = BUFFERSIZE;
        }
        
    }
    
    strcat((char *)&data_string[0], (const char *)buffer_uart_rx);
    
    USART_IntDisable(Usart1_Port, USART_IEN_RXDATAV);
    buff_rx_cnt = 0;
    usart_rx_flag = false;
    
    return SUCCESS;
}

/***************************************************************************//**
 * @brief
 *   USART1 TX Interrupt handler.
 *
 * @details
 *   UART Interrupt handler for USART1.
 *
 * @note
 *   Try to make this code as short as possible. Set the flag to false once the
 *      transmission is completed.
 *
 * @param[in] none
 *
 * @return
 *   Nothing, it's an interrupt function for fuck's sake.
 ******************************************************************************/
void USART1_TX_IRQHandler(void)
{
    /* Check TX buffer level status */
    if (Usart1_Port->IF & USART_IF_TXBL)
    {
        if ((false != usart_tx_flag) && (buff_tx_len > buff_tx_ap))
        {
            /* Transmit pending character */
            USART_Tx(Usart1_Port, *(buffer_uart_tx + buff_tx_ap++));
        }else{
            buff_tx_ap = 0;
            usart_tx_flag = false;
            USART_IntDisable(Usart1_Port, USART_IEN_TXBL);
        }
    }
}

/**************************************************************************//**
 * @brief UART1 RX IRQ Handler
 *
 * Set up the interrupt prior to use
 *
 * Note that this function handles overflows in a very simple way.
 *
 *****************************************************************************/
void USART1_RX_IRQHandler(void)
{
    /* Check for RX data valid interrupt */
    if (Usart1_Port->IF & USART_IF_RXDATAV)
    {
        usart_rx_flag = true;
        /* Copy data into RX Buffer */
        *(buffer_uart_rx + buff_rx_ap++) = USART_Rx(Usart1_Port);
        buff_rx_cnt++;
        if(buff_rx_ap >= BUFFERSIZE)
        {
            buff_rx_ap = 0;
        }
    }
}