/*******************************************************************************
 * systick_func.h
 *
 *  Created on: 20/09/2019
 *      Author: AntoMota
 ******************************************************************************/

#ifndef __SYSTICK_FUNC_H__
#define __SYSTICK_FUNC_H__

/* Libraries to be included */
#include "stdint.h"
#include "stdbool.h"

/* Function prototypes of the source file this header file references to */
uint8_t systick_conf(uint32_t miliseconds);
uint8_t systick_toggle(bool systick_status);

#endif /* __SYSTICK_FUNC_H__ */
/*** end of file ***/
