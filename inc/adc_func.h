/*******************************************************************************
 * adc_func.h
 *
 *  Created on: 11/10/2019
 *  Modified on: 11/10/2019
 *      Author: AntoMota
 ******************************************************************************/

#ifndef __ADC_FUNC_H__
#define __ADC_FUNC_H__

/* Libraries to be included */
#include "stdint.h"

/* Function prototypes of the source file this header file references to */
uint8_t adc_conf();
uint32_t adc_read();




#endif /* __ADC_FUNC_H__ */
/*** end of file ***/
