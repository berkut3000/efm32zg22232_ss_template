/*******************************************************************************
 * energy_func.h
 *
 *  Created on: 20/09/2019
 *  Modified on: 20/09/2019
 *      Author: AntoMota
 ******************************************************************************/

#ifndef __ENERGY_FUNC_H__
#define __ENERGY_FUNC_H__

/* Libraries to be included */
#include "stdint.h"

/* Function prototypes of the source file this header file references to */
uint8_t energy_mode_1();
uint8_t energy_mode_2();
uint8_t energy_mode_3();
uint8_t energy_mode_4();

#endif /* __ENERGY_FUNC_H__ */
/*** end of file ***/
