/*
 * delay_func.h
 *
 *  Created on: DD/MM/YYYY
 *      Author: hydro
 */

#ifndef __DELAY_FUNC_H__
#define __DELAY_FUNC_H__

/* Libraries to be included */
#include <stdint.h>

/* Function prototypes of the source file this header file references to */

uint8_t delay_c(uint32_t n_delays);

#endif /* __DELAY_FUNC_H__ */
/** end of file ***/