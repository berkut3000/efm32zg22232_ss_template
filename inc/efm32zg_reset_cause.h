/*
 * efm32zg_reset_cause.h
 *
 *  Created on: 11/03/2019
 *      Author: AntoMota
 */

#ifndef __EFM32ZG_RESET_CAUSE_H__
#define __EFM32ZG_RESET_CAUSE_H__

/* Libraries to be included */
#include <stdint.h>

/* Function prototypes of the source file this header file references to */
uint8_t get_reset_cause();
uint8_t print_reset_cause(uint32_t reset_flags);

#endif /* __EFM32ZG_RESET_CAUSE_H__ */
