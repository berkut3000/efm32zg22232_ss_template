/******************************************************************************
 * @ file  leuart_func.h
 *
 *  Created on: 31/10/2018
 *  Modified on: 27/09/2019
 *      Author: AntoMota
 ******************************************************************************/
#ifndef LEUART_FUNC_H
#define LEUART_FUNC_H

/** LEUART Rx/Tx Port/Pin Location */
#define LEUART_LOCATION     0
#define LEUART_TXPORT       gpioPortD       /* LEUART transmission port */
#define LEUART_TXPIN        4               /* LEUART transmission pin  */
#define LEUART_RXPORT       gpioPortD       /* LEUART reception port    */
#define LEUART_RXPIN        5               /* LEUART reception pin     */

#include <stdint.h>

void leuart_setup(void);
uint8_t leuart_dma_setup();
uint8_t leuart_string_tx(uint8_t * data_string);
uint8_t leuart_dma_rx(uint8_t * data_buff);
uint8_t leuart_dma_rx_timeout(uint8_t * data_buff, uint16_t time_out);

/* Deprecated and soon to be removed */
uint8_t leuart_rx(uint8_t * data_buff, uint8_t data_buff_size);
uint8_t leuart_rx_sc(uint8_t * data_buff, uint32_t data_buff_size,
        uint8_t data_stop, uint8_t data_stop_count);

#endif /* LEUART_FUNC_H */
/* end of file */