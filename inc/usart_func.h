/*******************************************************************************
 * usart_func.h
 *
 *  Created on: 20/11/2018
 *      Author: Ernesto Moises
 ******************************************************************************/

#ifndef  __USART_FUNC_H__
#define  __USART_FUNC_H__

#include <stdint.h>

/* Function prototypes */
uint8_t cmu_setup();
uint8_t uart_setup(uint32_t * bd, uint8_t * loc);
uint8_t uart_bytes_tx(uint8_t * data_string, uint8_t * data_len);
uint8_t uart_string_tx(uint8_t * data_string);
uint8_t uart_rx(uint8_t * data_string);
uint8_t uart_rx_timeout(uint8_t * data_string, uint32_t * t_out);
uint8_t uart_rx_no_hang(uint8_t * data_string);
uint8_t uart_rx_timeout_no_hang(uint8_t * data_string, uint32_t * t_out);

#endif /* __USART_FUNC_H__ */