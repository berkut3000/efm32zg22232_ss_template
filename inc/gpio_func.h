/******************************************************************************
 * @ file  gpio_func.h
 *
 *  Created on: 31/10/2018
 *      Author: AntoMota
 ******************************************************************************/

#ifndef __GPIO_FUNC_H__
#define __GPIO_FUNC_H__

/* Libraries to be included */
#include <stdint.h>
#include "em_gpio.h" /* To support GPIO_Port_TypeDef */

typedef struct {
	GPIO_Port_TypeDef port;
	uint8_t pin;	
}gpio_struct_t;

extern gpio_struct_t led0_struct;
extern gpio_struct_t led1_struct;

extern gpio_struct_t pbutton_struct;

extern gpio_struct_t gps_reset_struct;
extern gpio_struct_t gps_wku_struct;
extern gpio_struct_t gps_onoff_struct;

extern gpio_struct_t rn2903_reset_struct;


/* Function prototypes of the source file this header file references to */

uint8_t gpio_conf(void);
uint8_t gpio_set(gpio_struct_t * gpio_pin, bool gpio_state);
uint8_t gpio_get(gpio_struct_t * gpio_pin);
uint8_t gpio_toggle(gpio_struct_t * gpio_pin);

#endif /* __GPIO_FUNC_H__ */
/*** end of file ***/